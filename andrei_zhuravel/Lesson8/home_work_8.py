from decorators import before_and_after, decor_sum, decor_uppercase,\
    func_name, change, timer, typed

# Декораторы:
# Task 1. Напишите декоратор, который печатает перед и после вызова функции
# слова “Before” and “After”


@before_and_after
def our_text():

    """
    printing the text
    """
    print("text")


our_text()

# Task 2. Напишите функцию декоратор, которая добавляет 1 к заданному числу


@decor_sum
def func_number(x):
    """
    With help of decorator we add 1 to our number and return him
    """
    return x


print(func_number(5))

# Task 3. Напишите функцию декоратор, которая переводит полученный
# текст в верхний регистр


@decor_uppercase
def uppercase(text):
    """
    A function that converts the received text to uppercase
    """
    return text


print(uppercase("hello"))

# Task 4. Напишите декоратор func_name, который выводит на экран
# имя функции (func.__name__)


@func_name
def function_name():
    """
    Display the name of the function
    """
    x = "I am here"
    return x


print(function_name())

# Task 5. Напишите декоратор change, который делает так,
# что задекорированная функция принимает все свои не именованные
# аргументы в порядке, обратном тому, в котором их передали


@change
def reverse(x, y):
    """
    The decorated function accepts all of its unnamed arguments
    in the reverse order of the one in which they were passed
    And then we perform our function
    """
    result = x - y
    return result


print(reverse(2, 4))

# Task 6. Напишите декоратор, который вычисляет время работы
# декорируемой функции (timer)


@timer
def running_time(x, y):
    """
    Calculate the running time of the function
    """
    result = x / y + x * y
    return result


print(running_time(2, 4))

# Task 7. Напишите функцию, которая вычисляет значение числа Фибоначчи
# для заданного количество элементов последовательности и возвращает его
# и оберните ее декоратором timer и func_name


@timer
@func_name
def fib():
    """
    Calculates the value of the Fibonacci number for a given number
    of elements of the sequence and returns it
    """
    x = 0
    y = 1
    n = 0
    while n < 5:
        print(x)
        n += 1
        x, y = y, x + y


fib()

# Task 8. Напишите функцию, которая вычисляет сложное математическое выражение
# и оберните ее декоратором из пункта 1, 2


@before_and_after
@decor_sum
def func_skill(x, y):
    """
    A function that calculates a complex mathematical expression
    and wrap it with the decorator from paragraph 1, 2
    """
    return (abs(x) - abs(y)) * (abs(x) + abs(y))

# Task 9. Напишите декоратор, который проверял бы тип параметров функции,
# конвертировал их если надо и складывал:

# @typed(type=’str’)
# def add_two_symbols(a, b):
#    return a + b
#
# add_two_symbols("3", 5) -> "35"
# add_two_symbols(5, 5) -> "55"
# add_two_symbols('a', 'b') -> 'ab’
#
# @typed(type=’int’)
# def add_three_symbols(a, b, с):
#    return a + b + с
#
# add_three_symbols(5, 6, 7) -> 18
# add_three_symbols("3", 5, 0) -> 8
# add_three_symbols(0.1, 0.2, 0.4) -> 0.7000000000000001


@typed(type="str")
def add_two_symbols(a, b):
    """
    we check the type of two parameters (str),
    if necessary we convert and add,
    all with the help of a decorator
    """
    return a + b


@typed(type="int")
def add_three_symbols(a, b, с):
    """
    we check the type of three parameters (int),
    if necessary we convert and add,
    all with the help of a decorator

    """
    return a + b + с
