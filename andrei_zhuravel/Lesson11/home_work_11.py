# Ферма.
# Суть задания:
# На ферме могут жить животные и птицы
# И у тех и у других есть два атрибута: имя и возраст
# При этом нужно сделать так, чтобы возраст нельзя было изменить извне
# напрямую, но при этом можно было бы получить его значение
# У каждого животного должен быть обязательный метод – ходить
# А для птиц – летать
# Эти методы должны выводить сообщения (контекст придумайте)
# Также должны быть общие для всех классов обязательные методы – постареть
# - который увеличивает возраст на 1 и метод голос
# При обращении к несуществующему методу или атрибуту животного или птицы
# должен срабатывать пользовательский exception NotExistException и выводить
# сообщение о том, что для текущего класса (имя класса), такой метод или
# атрибут не существует – (имя метода или атрибута)
# На основании этих классов создать отдельные классы для Свиньи, Гуся,
# Курицы и Коровы – для каждого класса метод голос должен выводить разное
# сообщение (хрю, гага, кудах, му)
# После этого создать класс Ферма, в который можно передавать список животных
# Должна быть возможность получить каждое животное, живущее на ферме
# как по индексу, так и через цикл for.
# Также у фермы должен быть метод, который увеличивает возраст всех животных,
# живущих на ней.


class NotExistException(Exception):
    """
    Class to change default error to custom
    with additional message
    """
    message = 'Specified attribute/method not exist'

    def __init__(self, value):
        """
        :param value: Object attribute or method
        """
        self.final_message = f'{self.message}, attribute name: {value}'
        super().__init__(self.final_message)


class Animals:
    """
    Parent class - animals
    """
    voice = None
    kind = None

    def __init__(self, name, age, going='walk'):
        """
        :param name: Animal name
        :param age: Animal age
        :param going: Animal going mode
        """
        self.name = name
        self.__age = age
        self.going = 'walk'

    def animals_going(self) -> str:
        """
        :return: String with animal name and type of moving
        """
        if self.going == 'walk':
            return f'{self.name.title()} walking'
        else:
            return f'{self.name.title()} flying'

    @property
    def current_age(self) -> int:
        """
        :return: Current animal age
        """
        return self.__age

    @current_age.setter
    def current_age(self, value=1):
        """
        :param value: Number of years
        :return: None
        """
        self.__age += 1

    def grow_old_animal(self) -> int:
        """
        :return: Current animal age
        """
        self.__age += 1
        return self.__age

    def get_voice(self) -> str:
        """
        :return: String with animal name and animal voice
        """
        return f'{self.name} say: {self.voice}'

    def __getattr__(self, attr):
        """
        :param attr: Object attribute or method
        :return: Object attribute or method or
        error is appeared if method not exist
        """
        if isinstance(attr, Animals):
            return self.attr
        else:
            raise NotExistException(attr)

    def __str__(self) -> str:
        """
        :return: String with animal kind and name
        """
        return f'{self.kind.title()} {self.name.title()}'


class Pigs(Animals):
    """
    Class to create animal: Pigs
    """
    voice = 'Oi-Oi'
    kind = 'pig'


class Cows(Animals):
    """
    Class to create animal: Cows
    """
    voice = 'Moo-oo'
    kind = 'cow'


class Hens(Animals):
    """
    Class to create animal: Hens
    """
    voice = 'cluck'
    kind = 'hen'

    def __init__(self, name, age, going='fly'):
        super().__init__(name, age, going='fly')
        self.going = 'fly'


class Geese(Animals):
    """
    Class to create animal: Geese
    """
    voice = 'Quack-Quack'
    kind = 'goose'

    def __init__(self, name, age, going='fly'):
        super().__init__(name, age, going='fly')
        self.going = 'fly'


class Farm:
    """
    We create farm with different animals
    """
    def __init__(self, *animals):
        """
        :param animals: Animal objects
        """
        self.animals = animals
        self.index = 0

    def __str__(self) -> str:
        """
        :return: String with kind of animal and name
        """
        return ', '.join([f'{f.kind} {f.name}' for f in self.animals])

    def __contains__(self, animal) -> bool:
        """
        :param  animal: Animals object
        :return: True or False
        """
        return animal in self.animals

    def __iter__(self):
        """
        :return: Iterated object
        """
        return iter(self.animals)

    def __next__(self):
        """
        :return: Next collection item or
        error when collection is completed
        """
        try:
            item = self.animals[self.index]
        except IndexError:
            raise StopIteration()
        self.index += 1
        return item

    def __getitem__(self, index):
        """
        :param index: Number of index
        :return: Value which corresponds specified index
        """
        return self.animals[index]

    def grow_old_all(self) -> dict:
        """
        Increases age for all animals at farm for 1 (year)
        :return: Dictionary with animals names and increased age
        """
        list(map(Animals.grow_old_animal, self.animals))
        return dict((i.name, i.current_age) for i in self.animals)


def get_animals():
    cow = Cows('Zorka', 10)
    pig = Pigs('Sebastian', 15)
    goose = Geese('Zhora', 5)
    hen = Hens('Klava', 3)
    return cow, pig, goose, hen


if __name__ == "__main__":
    c1, p1, g1, h1 = get_animals()
    frm = Farm(c1, p1, g1, h1)

    # get animals walking (bird)
    print(h1.animals_going())
    # get animals walking
    print(c1.animals_going())
    # get voice
    print(g1.get_voice())
    print(p1.get_voice())
    # get current age
    print(p1.current_age)
    # +1 year for separate animal
    p1.current_age = 1
    print(p1.current_age)
    # get animals on Farm
    print(frm)
    # custom error for non exist attribute
    # print(p1.none)
    # get animals by index
    print(frm[3])
    # get items using 'for'
    for i in frm:
        print(i)
    # grow up all animals at farm
    print(frm.grow_old_all())
