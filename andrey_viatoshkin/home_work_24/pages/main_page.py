from andrey_viatoshkin.home_work_24.pages.base_page import BasePage


class MainPage(BasePage):
    URL = 'http://selenium1py.pythonanywhere.com/en-gb/'

    def __init__(self, driver):
        super().__init__(driver, self.URL)
