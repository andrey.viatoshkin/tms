from abc import ABC, abstractmethod
"""Суть задания:
На ферме могут жить животные и птицы
И у тех и у других есть два атрибута: имя и возраст
При этом нужно сделать так, чтобы возраст нельзя было изменить
извне напрямую, но при этом можно было бы получить его значение
У каждого животного должен быть обязательный метод – ходить
А для птиц – летать
Эти методы должны выводить сообщения (контекст придумайте)
Также должны быть общие для всех классов обязательные
методы – постареть - который увеличивает возраст на 1 и метод голос

При обращении к несуществующему методу или атрибуту животного
или птицы должен срабатывать
пользовательский exception NotExistException
и выводить сообщение о том, что для текущего класса (имя класса),
такой метод или атрибут не существует – (имя метода или атрибута)
На основании этих классов создать отдельные классы для
Свиньи, Гуся, Курицы и Коровы
– для каждого класса метод голос должен
выводить разное сообщение (хрю, гага, кудах, му)
После этого создать класс Ферма,
в который можно передавать список животных
Должна быть возможность получить каждое животное,
живущее на ферме как по индексу, так и через цикл for.
Также у фермы должен быть метод, который
увеличивает возраст всех животных, живущих на ней.
"""


class Bird(ABC):
    def __init__(self, name, age):
        self.name = name
        self._age = age

    def __getattr__(self, attr):
        raise NotExistException(f'{self.__class__.__name__} has no '
                                f'attribute {attr}')

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, age):
        self._age = age

    @abstractmethod
    def fly(self):
        print('Bird can fly')

    @abstractmethod
    def voice(self):
        print('Bird can sing')

    def be_older(self):
        self._age += 1


class Animal(ABC):
    def __init__(self, name, age):
        self.name = name
        self._age = age

    def __getattr__(self, attr):
        raise NotExistException(f'{self.__class__.__name__} has no '
                                f'attribute {attr}')

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, age):
        self._age = age

    @abstractmethod
    def move(self):
        print('Animal can move')

    @abstractmethod
    def voice(self):
        print('Animal can roar')

    def be_older(self):
        self._age += 1


class Cow(Animal):
    def move(self):
        print('Cow can move')

    def voice(self):
        print('Cow can moo')

    def __str__(self):
        return f'{self.name}, {Cow.__name__}'


class Pig(Animal):

    def move(self):
        print('Pig can move')

    def voice(self):
        print('Pig can oink')

    def __str__(self):
        return f'{self.name}, {Pig.__name__}'


class Goose(Bird):

    def fly(self):
        print('Goose can fly')

    def voice(self):
        print('Goose can quack')

    def __str__(self):
        return f'{self.name}, {Goose.__name__}'


class Chicken(Bird):
    def fly(self):
        print('Chicken can fly')

    def voice(self):
        print('Chicken can cluck')

    def __str__(self):
        return f'{self.name}, {Chicken.__name__}'


class Farm:
    def __init__(self, *args):
        self.args = args

    def __getitem__(self, index):
        return self.args[index]

    def __iter__(self):
        return iter(self.args)

    @staticmethod
    def be_older(*args):
        for z in args:
            z.age += 1


cow1 = Cow('Lili', 5)
pig1 = Pig('Pepa', 3)
goose1 = Goose('Gaga', 2)
chicken1 = Chicken('Lu', 1)

farm = Farm(cow1, pig1, goose1, chicken1)
print(farm[0])
farm.be_older(cow1, pig1, goose1, chicken1)
for i in farm:
    print(i, f'{i.age} years old')


class NotExistException(Exception):
    pass
