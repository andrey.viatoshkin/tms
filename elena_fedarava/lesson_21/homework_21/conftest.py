import pytest
from selenium import webdriver

MAIN_PAGE_URL = 'http://the-internet.herokuapp.com/'


@pytest.fixture(scope='module')
def driver():
    driver = webdriver.Chrome()
    yield driver


@pytest.fixture()
def open_main_page(driver):
    driver.get(MAIN_PAGE_URL)
    return driver
