# Task1: создайте класс работник
# У класса есть конструктор, в который надо передать имя и ЗП.
# У класса есть 2 метода: получение имени сотрудника и получение количества
# созданных работников.
# При создании экземпляра класса должен вызываться метод, который отвечает за
# увеличение количества работников на 1.


class Worker:

    counter = 0

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
        Worker.increase_counter()

    def get_name(self):
        return self.name

    @staticmethod
    def increase_counter():
        Worker.counter += 1

    @classmethod
    def get_number_workers(cls):
        return cls.counter


w1 = Worker('Alex', 1000)
print(w1.get_name())
w2 = Worker('Mary', 1000)
print(w2.get_name())
w3 = Worker('Frank', 1000)
print(w3.get_name())
w4 = Worker('Joy', 1000)
print(w4.get_name())
print(Worker.get_number_workers())


# Task2: Вы идете в путешествие, надо подсчитать сколько у денег у каждого
# студента. Класс студент описан следующим образом:
# class Student:
#     def __init__(self, name, money):
#         self.name = name
#         self.money = money
# Необходимо понять у кого больше всего денег и вернуть его имя. Если у
# студентов денег поровну вернуть: “all”.
# (P.S. метод подсчета не должен быть частью класса)
# assert most_money([ivan, oleg, sergei]) == "all"
# assert most_money([ivan, oleg]) == "ivan"
# assert most_money([oleg]) == "Oleg"


class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money


ivan = Student('ivan', 300)
oleg = Student('oleg', 500)
sergei = Student('sergei', 200)


def most_money(args: list):
    money_lst = [student.money for student in args]
    max_amount = max(money_lst)
    if money_lst.count(max_amount) == len(money_lst) and len(money_lst) != 1:
        return 'all'
    else:
        for i in args:
            if i.money == max_amount:
                return i.name


assert most_money([oleg]) == "oleg"
