from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

CHECKBOX = '//div[@id="checkbox"]'
REMOVE_BUTTON = '//button[@onclick="swapCheckbox()"]'
MESSAGE_1 = '//p[contains(text(),"s gone!")]'
ENABLE_BUTTON = '//button[@onclick="swapInput()"]'
MESSAGE_2 = '//p[contains(text(),"s enabled!")]'
INPUT_FIELD = '//input'

IFRAME_LINK = '//a[text()="iFrame"]'
IFRAME = '#mce_0_ifr'
PARAGRAPH = '//p'


def test_dynamic_controls(open_dynamic_controls):
    driver = open_dynamic_controls
    driver.find_element(By.XPATH, CHECKBOX)
    assert CHECKBOX
    driver.find_element(By.XPATH, REMOVE_BUTTON).click()
    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH, MESSAGE_1)))
    WebDriverWait(driver, 10).until(
        EC.invisibility_of_element_located((By.XPATH, CHECKBOX)))
    inp = driver.find_element(By.XPATH, INPUT_FIELD)
    assert inp.is_enabled() is False
    driver.find_element(By.XPATH, ENABLE_BUTTON).click()
    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH, MESSAGE_2)))


def test_iframe(open_iframes):
    driver = open_iframes
    driver.find_element(By.XPATH, IFRAME_LINK).click()
    iframe = driver.find_element(By.CSS_SELECTOR, IFRAME)
    driver.switch_to.frame(iframe)
    paragraph = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH, PARAGRAPH)))
    assert paragraph.text == 'Your content goes here.'
