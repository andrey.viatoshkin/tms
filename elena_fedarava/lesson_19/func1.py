def verify_lower_case(arg):
    """1ая – проверяет что все буквы в переданном слове в нижнем регистре и
    возвращает True либо False"""
    flag = True
    if arg.lower() != arg:
        flag = False
    return flag
