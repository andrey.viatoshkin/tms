"""В классическом варианте игра рассчитана на двух игроков. Каждый из игроков
 задумывает и записывает тайное 4-значное число с неповторяющимися цифрами.
Игрок, который начинает игру по жребию, делает первую попытку отгадать число.
Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое
противнику. Противник сообщает в ответ, сколько цифр угадано без совпадения
с их позициями в тайном числе (то есть количество коров) и сколько угадано
вплоть до позиции в тайном числе (то есть количество быков).
При игре против компьютера игрок вводит комбинации одну за другой, пока не
отгадает всю последовательность. Ваша задача реализовать программу, против
которой можно сыграть в "Быки и коровы"
Тестовые данные:
3129 - все совпадает
9123 - 4 коровы, 0 быков
8405 - все мимо
2310 - 2 коровы, 1 бык
3218 - 0 коров, 3 быка
"""
right_number = '3219'
answer = input('Введите число: ')
cows_counter = 0
bulls_counter = 0
index_counter = 0
for i in answer:
    if i in right_number and answer.index(i) != right_number.index(i):
        cows_counter += 1
        index_counter += 1
    if i in right_number and answer.index(i) == right_number.index(i):
        bulls_counter += 1
if 0 < cows_counter < 4 or 0 < bulls_counter < 4:
    print(f'{cows_counter} коровa(ов), {bulls_counter} бык(ов)')
elif cows_counter == 4 and index_counter == 4:
    print(f'{cows_counter} коровa(ов), {bulls_counter} бык(ов)')
elif bulls_counter == 4:
    print('Вы выиграли!')
else:
    print('Неверно!')
