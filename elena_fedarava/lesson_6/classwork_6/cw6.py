from datetime import datetime


# Task1: Напишите функцию, которая возвращает строку: “Hello world!”
def f():
    return 'Task 1: Hello world!'


print(f())


# Task2: Напишите функцию, которая вычисляет сумму трех чисел и возвращает
# результат в основную ветку программы
def f1(a1, a2, a3):
    return a1 + a2 + a3


print('Task 2:', f1(1, 2, 3))


# Task3: Придумайте программу, в которой из одной функции вызывается вторая.
# При этом ни одна из них ничего не возвращает в основную ветку программы,
# обе должны выводить результаты своей работы с помощью функции print().
def f2():
    print('Task 3: Test1')


def f3():
    print('Task 3: Test2')
    f2()


f3()


# Task4: Напишите функцию, которая не принимает отрицательные числа. и
# возвращает число наоборот.
# 21445 => 54421
# 123456789 => 987654321
def reverse(x):
    if x >= 0:
        return str(x)[::-1]
    else:
        print('Task 4: Число меньше 0!')


print('Task 4:', reverse(123456789))


# Task5: Напишите функцию fib(n), которая по данному целому неотрицательному
# n возвращает n-e число Фибоначчи
def fib(n):
    first_elem = 0
    second_elem = 1
    im = n - 2
    while im > 0:
        next_elem = first_elem + second_elem
        first_elem = second_elem
        second_elem = next_elem
        im -= 1
    if n > 2:
        return next_elem
    elif n == 1:
        return 0
    elif n == 2:
        return 1


print('Task 5:', fib(8))

# Task6: Напишите функцию, которая проверяет на то, является ли строка
# палиндромом или нет.
str1 = 'а роза упала на лапу азора'


def pal(p):
    p = p.replace(' ', '')
    if p == p[::-1]:
        print('Task 6: Это палиндром!')
    else:
        print('Task 6: Это не палиндром!')


pal(str1)


# Task7: У вас интернет магазин, надо написать функцию которая проверяет что
# введен правильный купон и он еще действителен
# def check_coupon(entered_code, correct_code, current_date, expiration_date):
# check_сoupon("123", "123", "July 9, 2015", "July 9, 2015")  == True
# check_сoupon("123", "123", "July 9, 2015", "July 2, 2015")  == False
def check_coupon(entered_code, correct_code, current_date, expiration_date):
    date_formatter = "%B %d, %Y"
    current_date = datetime.strptime(current_date, date_formatter)
    expiration_date = datetime.strptime(expiration_date, date_formatter)
    if entered_code == correct_code:
        if current_date <= expiration_date:
            print('Task 7:', True)
        else:
            print('Task 7:', False)
    else:
        print('Task 7:', False)


check_coupon('123', '123', "July 9, 2015", 'July 9, 2015')


# Task8: Фильтр. Функция принимает на вход список, проверяет есть ли эти
# элементы в списке exclude, если есть удаляет их и возвращает список с
# оставшимися элементами
# exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]
# Examples:
# filter_list(["Mallard", "Hook Bill", "African", "Crested", "Pilgrim",
# "Toulouse", "Blue Swedish"]) => ["Mallard", "Hook Bill", "Crested",
# "Blue Swedish"]
# ["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"] =>
# ["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"]
# ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"] => []
def filter_list(lst1):
    exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim",
               "Steinbacher"]
    copy_filter = lst1.copy()
    for i in lst1:
        if i in exclude:
            copy_filter.remove(str(i))
    return copy_filter


print('Task 8:', filter_list(["African", "Roman Tufted", "Toulouse", "Pilgrim",
                              "Steinbacher"]))
