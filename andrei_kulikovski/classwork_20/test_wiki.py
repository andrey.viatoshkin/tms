import pytest
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


MAIN_PAGE_URL = "https://en.wikipedia.org/wiki/Main_Page"

search_field_id = "searchInput"
search_header_id = "firstHeading"


@pytest.mark.search
def test_search_field_main_page(prepare_driver, open_main_page):
    driver = prepare_driver
    search_field = driver.find_element(By.ID, search_field_id)
    search_field.send_keys("Python")
    time.sleep(2)
    search_field.send_keys(Keys.ENTER)
    search_header = driver.find_element(By.ID, search_header_id)
    assert search_header.text == "Python", "Python page was not opened"
