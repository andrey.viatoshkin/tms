import pytest
import time
from selenium import webdriver
from andrei_kulikovski.classwork_20.test_wiki import MAIN_PAGE_URL


@pytest.fixture(scope="session")
def prepare_driver():
    driver = webdriver.Chrome(executable_path="D:/QA python/Python/"
                                              "chromedriver")
    yield driver
    time.sleep(2)
    driver.quit()


@pytest.fixture()
def open_main_page(prepare_driver):
    return prepare_driver.get(MAIN_PAGE_URL)
