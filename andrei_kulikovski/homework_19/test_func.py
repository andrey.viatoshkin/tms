import pytest
from andrei_kulikovski.homework_19.func_1 import islower_str
from andrei_kulikovski.homework_19.func_2 import float_num, InputTypeError
from andrei_kulikovski.homework_19.func_3 import detect_special_symbl


@pytest.mark.repeat(3)
@pytest.mark.parametrize("islower_arg, expected",
                         [("yyy", True), ("HHH", False), ("rrR", False)])
def test_islower_str(islower_arg, expected):
    assert islower_str(islower_arg) == expected


@pytest.mark.repeat(3)
@pytest.mark.parametrize("float_num_arg, expected",
                         [(100, 100.0), (15, 15.0), (33, 33.0)])
def test_float_num(float_num_arg, expected):
    assert float_num(float_num_arg) == expected


@pytest.mark.repeat(3)
@pytest.mark.xfail
@pytest.mark.parametrize("float_num_arg, expected",
                         [("33", InputTypeError),
                          ("a", InputTypeError), (33, 33.0)])
def test_float_num_raise(float_num_arg, expected):
    with pytest.raises(InputTypeError):
        assert float_num(float_num_arg) == expected


@pytest.mark.repeat(3)
@pytest.mark.parametrize("detect_special_symbl_arg, expected",
                         [("#", True), ("&", True), ("a", False)])
def test_detect_special_symbl(detect_special_symbl_arg, expected):
    assert detect_special_symbl(detect_special_symbl_arg) == expected
