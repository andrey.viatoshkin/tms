import requests


class Endpoints:
    PROJECTS = "projects"
    REPO_BRANCHES = "projects/{project_id}/repository/branches"
    REPO_COMMITS = "projects/{project_id}/repository/commits"
    COMMITS_SHA = "projects/{project_id}/repository/commits/" \
                  "{sha}"
    COMMITS_COMMENTS = "projects/{project_id}/repository/commits/" \
                       "{sha}/comments"


def check_response(response):
    response.raise_for_status()
    return response


class RestHelper:
    def __init__(self, url, auth_token=None):
        self.url = url
        self.auth_token = auth_token
        self.session = requests.Session()
        if auth_token:
            self.session.headers.update(
                {"Authorization": f"Bearer {self.auth_token}"})

    def do_get(self, endpoint):
        response = self.session.get(self.url + endpoint)
        return check_response(response).json()

    def do_post(self, endpoint, **kwargs):
        response = self.session.post(self.url + endpoint, **kwargs)
        return check_response(response).json()


class GitLabHelper:
    def __init__(self, auth_token, api_version="v4"):
        self.auth_token = auth_token
        self.api_version = api_version
        self.url = f"https://gitlab.com/api/{self.api_version}/"
        self.session = requests.Session()
        self.session.headers.update(
            {"Authorization": f"Bearer {self.auth_token}"})

    def get_projects(self):
        return check_response(self.session.get(
            self.url + Endpoints.PROJECTS)).json()

    def get_branches(self, project_id):
        return check_response(
            self.session.get(self.url + Endpoints.REPO_BRANCHES.format(
                project_id=project_id))).json()

    def create_branch(self, project_id, branch_name, ref):
        data = {
            "branch": branch_name,
            "ref": ref
        }
        return check_response(
            self.session.post(self.url + Endpoints.REPO_BRANCHES.format(
                project_id=project_id), json=data)).json()


class SecondGitLabHelper(RestHelper):
    def __init__(self, auth_token, api_version="v4"):
        self.auth_token = auth_token
        self.api_version = api_version
        self.url = f"https://gitlab.com/api/{self.api_version}/"
        super().__init__(self.url, auth_token)

    def get_projects(self):
        return self.do_get(Endpoints.PROJECTS)

    def get_branches(self, project_id):
        return self.do_get(Endpoints.REPO_BRANCHES.format(
            project_id=project_id))

    def create_branch(self, project_id, branch_name, ref):
        data = {
            "branch": branch_name,
            "ref": ref
        }
        return self.do_post(Endpoints.REPO_BRANCHES.format(
            project_id=project_id), json=data)

    def get_commits(self, project_id):
        return self.do_get(Endpoints.REPO_COMMITS.format(
            project_id=project_id))

    def get_commits_sha(self, project_id, sha):
        return self.do_get(Endpoints.COMMITS_SHA.format(project_id=project_id,
                                                        sha=sha))

    def get_commits_comments(self, project_id, sha):
        return self.do_get(Endpoints.COMMITS_COMMENTS.format(
            project_id=project_id, sha=sha))

    def post_commits_comments(self, project_id, comment, sha):
        data = {
            "note": comment
        }
        return self.do_post(Endpoints.COMMITS_COMMENTS.format(
            project_id=project_id, sha=sha), json=data)
