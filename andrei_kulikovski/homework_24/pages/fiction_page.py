from pages.base_page import BasePage
from pages.ficlion_page_locators import FictionPageLocators


class FictionPage(BasePage):
    URL = "http://selenium1py.pythonanywhere.com/" \
          "en-gb/catalogue/category/fiction_3/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_fiction_sub(self):
        return self.driver.find_element(*FictionPageLocators.FICTION_PAGE_SUB)

    def fiction_click(self):
        fiction = self.driver.find_element(*FictionPageLocators.FICTION_LINK)
        fiction.click()
