"""
проверяет, является ли переданный аргумент спец символом
"""


def check_spec_sym(string):
    order = ord(string)
    if (order < 48) \
        or ((order > 57) and (order < 65)) \
        or ((order > 90) and (order < 97)) \
            or order > 122:
        return True
    return False


if __name__ == '__main__':
    print(check_spec_sym("*"))
