# Шифр цезаря
# Шифр Цезаря — один из древнейших шифров. При шифровании каждый символ
# заменяется другим, отстоящим от него в алфавите на фиксированное число
# позиций.
# Примеры:
# hello world! -> khoor zruog!
# this is a test string -> ymnx nx f yjxy xywnsl
# Напишите две функции - encode и decode принимающие как параметр строку и
# число - сдвиг.
# Все взаимодействие с программой должно производиться через терминал!
# Привер:
# Выберите операцию:
# 1) Encode
# 2) Decode
# 2
# Введите фразу:
# И так далее!

import string


def caesar_cipher(text: str, key: int, decrypt=False) -> str:
    """
    this function is an encrypter/decrypter of the input string using caesar
    cipher depending on the parameter decrypt (encrypt = False, decrypt = True)
    """

    character_set = string.ascii_lowercase + string.ascii_uppercase
    n = len(character_set)

    if key < 0:
        key += n

    if decrypt:
        key = n - key

    table = str.maketrans(character_set, character_set[key:] +
                          character_set[:key])
    translated_text = text.translate(table)

    return translated_text


def interaction_cipher():
    """
    function that interacts with caesar_cipher()
    """
    operation = int(input("Выберите операцию:\n 1 - Encode \n 2 - Decode\n"))
    if (operation > 2) or (operation < 1):
        print("Неверный ввод")
        return
    my_string = input("Введите строку: ")
    key = int(input("Введите целочисленное значение ключа: "))

    if operation == 1:
        encrypted_str = caesar_cipher(my_string, key)
        return encrypted_str
    if operation == 2:
        decrypted_str = caesar_cipher(my_string, key, True)
        return decrypted_str
