from abc import ABC, abstractmethod
# На ферме могут жить животные и птицы
# И у тех и у других есть два атрибута: имя и возраст
# При этом нужно сделать так, чтобы возраст нельзя было изменить извне
# напрямую, но при этом можно было бы получить его значение
# У каждого животного должен быть обязательный метод – ходить
# А для птиц – летать
# Также должны быть общие для всех классов обязательные методы –
# постареть - который увеличивает возраст на 1 и метод голос


class NotExistException(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class Creature(ABC):
    def __init__(self, name, age):
        self._name = name
        self._age = age

    # При обращении к несуществующему методу или атрибуту животного или птицы
    # должен срабатывать пользовательский exception NotExistException и
    # выводить сообщение о том, что для текущего класса (имя класса), такой
    # метод или атрибут не существует – (имя метода или атрибута)
    def __getattr__(self, item):
        raise NotExistException(
            f'{__class__.__name__} object has no attribute {item}'
        )

    def __str__(self):
        return f'{self.name}, age: {self.age}'

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def age(self):
        return self._age

    def get_older(self):
        self._age += 1


class Animal(Creature):
    @abstractmethod
    def voice(self):
        raise NotImplementedError

    @abstractmethod
    def move(self):
        raise NotImplementedError


class Bird(Creature):
    @abstractmethod
    def voice(self):
        raise NotImplementedError

    @abstractmethod
    def fly(self):
        raise NotImplementedError


# На основании этих классов создать отдельные классы для Свиньи, Гуся, Курицы
# и Коровы – для каждого класса метод голос должен выводить разное сообщение
# (хрю, гага, кудах, му)
class Pig(Animal):
    def move(self):
        print('Свинья куда-то пошла')

    def voice(self):
        print('Хрю')


class Cow(Animal):
    def move(self):
        print('Корова куда-то пошла')

    def voice(self):
        print('Мууу')


class Goose(Bird):
    def fly(self):
        print('Гусь летит')

    def voice(self):
        print('Га-га')


class Chicken(Bird):
    def fly(self):
        print('Курица попыталась полететь')

    def voice(self):
        print('Куд-кудах')


# После этого создать класс Ферма, в который можно передавать список животных
# Должна быть возможность получить каждое животное, живущее на ферме как по
# индексу, так и через цикл for.
# Также у фермы должен быть метод, который увеличивает возраст всех животных,
# живущих на ней.
class Farm:
    def __init__(self, *creatures):
        self.creatures = creatures

    def __getitem__(self, index):
        return self.creatures[index]

    def __iter__(self):
        return iter(self.creatures)

    def get_older(self):
        for beast in self.creatures:
            beast.get_older()


if __name__ == "__main__":
    cow_1 = Cow('cow_1', 1)
    pig_1 = Pig('pig_1', 2)
    goose_1 = Goose('goose_1', 3)
    chicken_1 = Chicken('chicken_1', 4)

    my_farm = Farm(cow_1, pig_1, goose_1, chicken_1)
    print(f'Первый житель фермы: {my_farm[0]}')
    print('Все жители фермы:')
    for beasts in my_farm:
        print(beasts)

    my_farm.get_older()
    print('Прошёл год:')
    for beasts in my_farm:
        print(beasts)

    print('Возраст cow_1:', cow_1.age)
    print('Возраст pig_1:', pig_1.age)
