from selenium.webdriver.common.by import By


URL = "http://the-internet.herokuapp.com/"

"""Задание 1
1.	На главной странице перейти по ссылке Checkboxes
2.	Установить галочку для второго чекбокса и проверить что она установлена
3.	Для первого чекбокса убрать галочку и протестировать, что ее нет
"""


def test_checkbox(browser):
    browser.get(URL)
    browser.find_element(By.XPATH,
                         '//*[@id="content"]//*[text()="Checkboxes"]').click()
    check_1 = browser.find_element(By.XPATH, '//input[@type="checkbox"][1]')
    check_1.click()
    check_2 = browser.find_element(By.XPATH, '//input[@type="checkbox"][2]')
    check_2.click()
    assert not check_2.is_selected(), "Checkbox is selected"
    assert check_1.is_selected(), "Checkbox is not selected"


"""Задание 2
1.	На главной странице перейти по ссылке Multiple Windows
2.	После этого найти и нажать на ссылку Click here
3.	Убедиться, что открылось новое окно и проверить что заголовок страницы –
New window
"""


def test_multiple_window(browser):
    browser.get(URL)
    browser.find_element(By.XPATH, '//a[contains(@href,"windows")]').click()
    browser.find_element(By.XPATH,
                         '//a[contains(@href,"windows/new")]').click()
    browser.switch_to.window(browser.window_handles[1])
    title = browser.title
    # print(title)
    assert title == "New Window", "Title is not the same"


"""Задание 3
1.	На главной странице перейти по ссылке Add/Remove elements
2.	Проверить что при нажатии на кнопку Add element, появляется новый элемент
3.	Написать отдельный тест, который проверяет удаление элементов
"""


def test_add_or_remove_elements(browser):
    browser.get(URL)
    browser.find_element(By.XPATH,
                         '//a[contains(@href,"remove")]').click()
    browser.find_element(By.XPATH, '//div/button').click()
    delete_button = browser.\
        find_element(By.XPATH,
                     '//button[contains(@class,"added")]')
    assert delete_button.is_enabled(), "Button is available for clicking"


def test_check_delete_button(browser):
    browser.get(URL)
    browser.find_element(By.XPATH,
                         '//a[contains(@href,"remove")]').click()
    browser.find_element(By.XPATH, '//div/button').click()

    delete_button = browser.\
        find_element(By.XPATH, '//button[contains(@class,"added")]')
    delete_button.click()
    assert not delete_button.is_displayed(), "The button is not delete"
