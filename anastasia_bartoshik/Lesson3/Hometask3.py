import math
import random

# Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'.

msg = 'www.my_site.com#about'
print(msg.replace('#', '/'))

# В строке “Ivanov Ivan” поменяйте местами слова => "Ivan Ivanov".

s = 'Ivanov Ivan'
words = s.split(' ')
words[-1] = 'Ivanov'
words[0] = 'Ivan'
a = [words[0], words[-1]]
print(s, ' '.join(a), sep='\n')

# Напишите программу которая удаляет пробел в начале строки.

a = "  hello world! "
print(a.lstrip())

# Напишите программу которая удаляет пробел в конце строки.

s = " hello world! "
print(s.rstrip())

# a = 10, b=23, поменять значения местами, чтобы в переменную “a” было
# записано значение “23”, в “b” - значение “-10”

a = 10
b = 23
a = 10 * (-1)
a, b = b, a
print('Значение a', a)
print('Значение b', b)

# Значение переменной “a” увеличить в 3 раза, а значение “b” уменьшить на 3

a = 10
b = 23
a *= 3
b -= 3
print(a, b, sep='\n')

# Преобразовать значение “a” из целочисленного в число с плавающей точкой
# (float), а значение в переменной “b” в строку.

a = 10
b = '23'
print(type(float(a)), float(a))
print('String:', b)

# Разделить значение в переменной “a” на 11 и вывести результат с точностью
# 3 знака после запятой.

a = 10
a /= 11
print(round(a, 3))

# Преобразовать значение переменной “b” в число с плавающей точкой и записать в
# переменную “c”. Возвести полученное число в 3-ю степень.

b = 23
c = float(b)
print(pow(c, 3))

# Получить случайное число, кратное 3-м

a = random.randrange(0, 100, 3)
print(a)

# Получить квадратный корень из 100 и возвести в 4 степень


a = 100
b = math.sqrt(a)
print('Квадратный корень из 100:', math.sqrt(a))
print('Квадратный корень из 100 в 4 степени:', pow(b, 4))

# Строку “Hi guys” вывести 3 раза и в конце добавить “Today”.

a = 'Hi guys'
a *= 3
b = 'Today'
print(a + b)

# Получить длину строки из предыдущего задания.

s = 'Hi guysHi guysHi guysToday'
print(len(s))

# Взять предыдущую строку и вывести слово “Today” в прямом и обратном порядке.

s = 'Hi guysHi guysHi guysToday'
a = s[-5:]
print(s[-5:])
print(a[::-1])

# Hi guysHi guysHi guysToday” вывести каждую 2-ю букву в прямом и
# обратном порядке.

s = "Hi guysHi guysHi guysToday"

print(s.replace(' ', '')[1::2])
print(s.replace(' ', '')[-2::-2])

# Есть строка: “my name is name”. Напечатайте ее, но вместо 2ого
# “name” вставьте ваше имя.

s = 'my name is name'
words = s.split(' ')
words[-1] = 'Nastya'
print(s.replace('s name', 's Nastya'))

# Полученную строку в задании 12 вывести: а) Каждое слово с большой буквы
# б) все слова в нижнем регистре в) все слова в верхнем регистре.

s = 'Hi guysHi guysHi guysToday'
print(s.title())
print(s.lower())
print(s.upper())

# Посчитать сколько раз слово “Task” встречается в строке из задания 12.

s = 'Hi guys'
print(s.count('Task'))
