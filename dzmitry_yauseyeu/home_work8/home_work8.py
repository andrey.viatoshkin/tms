from functools import wraps
from timeit import default_timer

"""1. Напишите декоратор, который
печатает перед и после вызова функции
слова “Before” and “After”"""


def decorator(function_to_decorate):
    @wraps(function_to_decorate)
    def wrapper(*args, **kwargs):
        print("Before")
        if function_to_decorate is not None:
            function_to_decorate(*args, **kwargs)
        print("After")

    return wrapper


"""2.Напишите функцию декоратор,
которая добавляет 1 к заданному числу"""


def inc_decorator(function):
    @wraps(function)
    def my_wrapper(*args):
        print('Number:', function(*args))
        return f'Number after addition: {function(*args) + 1}'
    return my_wrapper


"""3. Напишите функцию декоратор, которая переводит
полученный текст в верхний регистр"""


def decorator2(function):
    @wraps(function)
    def wrapper():
        return f'Formatted text: {function().upper()}'
    return wrapper


"""4.Напишите декоратор func_name,
который выводит на экран имя функции (func.__name__) """


def decorator_fun_name(function_to_decorate):
    # Функция выфодит имя функции
    @wraps(function_to_decorate)
    def my_wrapper_of_text(f):
        function_to_decorate()
        print("Function name is {}".format(f.__name__))

    return my_wrapper_of_text


"""5. Напишите декоратор change, который делает так,
 что задекорированная функция принимает все свои
  не именованные аргументы в порядке, обратном
   тому, в котором их передали
"""


def change(func):
    @wraps(func)
    def wrapper(*args):
        args = args[::-1]
        return func(*args)
    return wrapper


"""6. Напишите декоратор, который вычисляет
время работы декорируемой функции (timer)"""


def timer(func):
    @wraps(func)
    def wrapper(*args):
        start_time = default_timer()
        func(*args)
        end_time = default_timer()
        return end_time - start_time
    return wrapper


@timer
def func_to_measure():
    """
        This function takes one argument and returns it. There is a rubbish
        inside function to make it more difficult and check the speed of a
        processor.
        """
    list_1 = [i**5 for i in range(100)]
    return list_1


result = func_to_measure()
print(result)


"""9. Напишите декоратор, который проверял бы тип
параметров функции, конвертировал их если надо и складывал:
"""

"это функция, которая принимает на вход строковое"
" значение type_to_conve_to"
" type_to_conve_to - строкое значение, которое содержит тип результата"
" строкое значение никак не связано с названиями типов в python"

"типы переменных в python:"
"int, float, double, str, list, dict, tuple..."


def decorator_converter(type_to_conver_to):
    # type(2) -> int
    def actual_decorator(func):
        @wraps(func)
        def wrapper(*elements):

            converted_tuple = tuple()
            # что мы передадим дальше с уже сконвертируемыми значениями

            if type_to_conver_to == "sting":
                # приводим все к строке и СКЛЕИВАЕМ полученные строки
                print("приводим все к строке и СКЛЕИВАЕМ полученные строки")

                for param in elements:

                    # конвертируем, если надо в str
                    if type(param) != str:
                        param = str(param)

                    # добавляем в convertedTuple

                    param = (param,)
                    converted_tuple = converted_tuple + param

            if type_to_conver_to == "digit":
                # приводим все к числу (целому или
                # нецелому) и СКЛАДЫВАЕМ полученные числа
                print("приводим все к числу (целому или "
                      "нецелому) и СКЛАДЫВАЕМ полученные числа")

                for param in elements:

                    # конвертируем, если надо в str
                    if type(param) != int and type(param) != float:
                        param = int(param)

                    # добавляем в convertedTuple
                    param = (param,)
                    converted_tuple = converted_tuple + param

            return func(*converted_tuple)

        return wrapper

    return actual_decorator
