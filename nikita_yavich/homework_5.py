import random
import string

# Быки и коровы
# В классическом варианте игра рассчитана на двух игроков. Каждый из игроков
# задумывает и записывает тайное 4-значное число с неповторяющимися цифрами.
# Игрок, который начинает игру по жребию, делает первую попытку отгадать
# число.
# Попытка — это 4-значное число с неповторяющимися цифрами,
# сообщаемое противнику. Противник сообщает в ответ, сколько цифр угадано
# без совпадения с их позициями в тайном числе (то есть количество коров)
# и сколько угадано вплоть до позиции в тайном числе (то есть количество
# быков). При игре против компьютера игрок вводит комбинации одну за другой,
# пока не отгадает всю последовательность.
# Ваша задача реализовать программу, против которой можно сыграть в "Быки и
# коровы"
# Пример
# Загадано число 3219
# >>> 2310
# Две коровы, один бык
# >>> 3219
# Вы выиграли!

temp_list = []
temp_list = [i for i in range(10) if i not in temp_list]
key_number = random.shuffle(temp_list)
key_number = [str(i) for i in temp_list[0:4]]
key_number = ''.join(key_number)
key_number_input = '0'
animals = {0: ['ноль коров', 'ноль быков'],
           1: ['одна корова', 'один бык'],
           2: ['две коровы', 'два быка'],
           3: ['три коровы', 'три быка'],
           4: ['четыре коровы', 'четыре быка']
           }
while key_number_input != key_number:
    key_number_input = input('Guess the number: ')
    cow_count = 0
    bull_count = 0
    if key_number_input == key_number:
        break
    for i in range(len(key_number)):
        for j in range(len(key_number_input)):
            if key_number[i] == key_number_input[j] and i == j:
                bull_count += 1
            elif key_number[i] == key_number_input[j]:
                cow_count += 1
    print(animals[cow_count][0], ', ', animals[bull_count][1])
print('Вы выйграли!')

# Like
# Создайте программу, которая, принимая массив имён, возвращает строку
# описывающая количество лайков (как в Facebook).
# Примеры:
# Введите имена через запятую: "Ann"
# -> "Ann likes this"
# Введите имена через запятую: "Ann, Alex"
# -> "Ann and Alex like this"
# Введите имена через запятую: "Ann, Alex, Mark"
# -> "Ann, Alex and Mark like this"
# Введите имена через запятую: "Ann, Alex, Mark, Max"
# -> "Ann, Alex and 2 others like this"
# Если ничего не вводить должен быть вывод:
# -> "No one likes this"

like_input = input('input names: ').split(',')
name = 'No one'
if len(like_input) == 1 and '' in like_input:
    print(f'{name} likes this')
elif len(like_input) == 1:
    print(f'{like_input[0]} likes this')
elif len(like_input) == 2:
    print(f'{like_input[0]} and {like_input[1]} like this')
elif len(like_input) == 3:
    print(f'{like_input[0]}, {like_input[1]} and {like_input[2]} like this')
elif len(like_input) > 3:
    print(f'{like_input[0]}, {like_input[1]} and {len(like_input) - 2} others '
          f'like this')

# BuzzFuzz
# Напишите программу, которая перебирает последовательность от 1 до 100. Для
# чисел кратных 3 она должна написать: "Fuzz" вместо печати числа, а для
# чисел кратных 5  печатать "Buzz". Для чисел которые кратны 3 и 5
# надо печатать "FuzzBuzz". Иначе печатать число.
# Вывод должен быть следующим:
# 1
# 2
# fuzz
# 4
# buzz
# fuzz
# 7
# 8
# ..

for i in range(1, 101):
    if i % 3 == 0 and i % 5 == 0:
        print('FuzzBuzz')
    elif i % 3 == 0:
        print('Fuzz')
    elif i % 5 == 0:
        print('Buzz')
    else:
        print(i)

# Напишите код, который возьмет список строк и пронумерует их.
# Нумерация начинается с 1, имеет “:” и пробел
# [] => []
# ["a", "b", "c"] => ["1: a", "2: b", "3: c"]

list_inp = list(map(str, input('Input elements separated by space: ').split()))
list_c = [str(list_inp.index(i) + 1) + ': ' + i for i in list_inp]
print(list_c)

# Проверить, все ли элементы одинаковые
#
# [1, 1, 1] == True
# [1, 2, 1] == False
# ['a', 'a', 'a'] == True
# [] == True

a = list(input('Input elements without separator '))
print(len(a) == a.count(a[0]))

# k = 1
# for i in a:
#     if i == a[0]:
#         k *= 1
#     else:
#         k *= 0
# if k == 1:
#     print(True)
# else:
#     print(False)

# Проверка строки. В данной подстроке проверить все ли буквы в строчном
# регистре или нет и вернуть список не подходящих.
# dogcat => [True, []]
# doGCat => [False, ['G', 'C']]

input_string = input('Input the string: ')
output_list = [True, []]
for i in input_string:
    if i not in string.ascii_lowercase:
        output_list[0] = False
        output_list[1].append(i)
print(output_list)

# Сложите все числа в списке, они могут быть отрицательными, если список
# пустой вернуть 0
# [] == 0
# [1, 2, 3] == 6
# [1.1, 2.2, 3.3] == 6.6
# [4, 5, 6] == 15
# range(101) == 5050

list_a = range(101)
print(sum(list_a))
