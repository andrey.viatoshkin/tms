# Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'
import random

site_string = 'www.my_site.com#about'
print(site_string[0:site_string.index('#')] + '/' + site_string[
    site_string.index('#') + 1:])

# В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"

ex_string = 'Ivanou Ivan'
ex_string_list = ex_string.split()
print(ex_string_list[1] + ' ' + ex_string_list[0])

# Напишите программу которая удаляет пробел в начале строки

third_string = input()
if third_string.startswith(' ') is True:
    third_string_1 = third_string[1:]
    print(third_string_1)
else:
    print(third_string)

# Напишите программу которая удаляет пробел в конце строки

fourth_string = input()
if fourth_string.endswith(' ') is True:
    fourth_string_2 = fourth_string[:-1]
    print(fourth_string_2)
else:
    print(fourth_string)

# a = 10, b=23, поменять значения местами, чтобы в переменную “a” было
# записано значение “23”, в “b” - значение “-10”

a = 10
b = 23
# c = a
# a = b
# b = c
a, b = b, a
print('a is now: ', a, 'b is now: ', b)

# значение переменной “a” увеличить в 3 раза, а значение “b” уменьшить на 3

a *= 3
b -= 3

# преобразовать значение “a” из целочисленного в число с плавающей точкой (
# float), а значение в переменной “b” в строку

a = float(a)
b = str(b)

# Разделить значение в переменной “a” на 11 и вывести результат с точностью
# 3 знака после запятой

print(round(a / 11), 3)

# Преобразовать значение переменной “b” в число с плавающей точкой и
# записать в переменную “c”. Возвести полученное число в 3-ю степень.

c = float(b)
print(pow(c, 3))

# Получить случайное число, кратное 3-м

random_number = random.randint(0, 1000000000000000000) * 3
print(random_number)

# Получить квадратный корень из 100 и возвести в 4 степень

number_11 = 100
number_11 = pow(number_11, (1 / 2))
number_11 = pow(number_11, 4)
print(number_11)

# Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
# “Hi guysHi guysHi guysToday”

string_12 = 'Hi guys'
string_12 = string_12 * 3
today_string = 'Today'
string_13 = string_12 + today_string
print(string_13)

# Получить длину строки из предыдущего задания

print(len(string_13))

# Взять предыдущую строку и вывести слово “Today” в прямом и обратном порядке

print(string_13[21:])
print(string_13[:20:-1])

# “Hi guysHi guysHi guysToday” вывести каждую 2-ю букву в прямом и обратном
# порядке

print(string_13[::2])
print(string_13[::-2])

# Используя форматирования подставить результаты из задания 10 и 11 в
# следующую строку
# “Task 10: <в прямом>, <в обратном> Task 11: <в прямом>, <в обратном>”

str_16_1 = str(random_number)
str_16_2 = str(random_number)[::-1]
str_16_3 = str(number_11)
str_16_4 = str(number_11)[::-1]
print('Task 10: ', str_16_1, str_16_2, 'Task 11: ', str_16_3, str_16_4)

# Есть строка: “my name is name”. Напечатайте ее, но вместо 2ого “name”
# вставьте ваше имя.

name_17 = 'my name is name'
name_word = 'name'
nikita_word = 'Nikita'
name_17 = name_17.replace(name_word, nikita_word, 2)
name_17 = name_17.replace(nikita_word, name_word, 1)
print(name_17)

# Полученную строку в задании 12 вывести:
# а) Каждое слово с большой буквы
# б) все слова в нижнем регистре
# в) все слова в верхнем регистре

print(string_12.title())
print(string_12.lower())
print(string_12.upper())

# Посчитать сколько раз слово “Task” встречается в строке из задания 12

print(string_12.count('Task'))
