"""
1. Свяжите переменную с любой строкой, состоящей не менее чем
из 8 символов. Извлеките из строки первый символ, затем последний,
третий с начала и третий с конца. Измерьте длину вашей строки.
2. Присвойте произвольную строку длиной 10-15 символов переменной
и извлеките из нее следующие срезы:
первые восемь символов
четыре символа из центра строки
символы с индексами кратными трем
переверните строку
3. Есть строка: “my name is name”. Напечатайте ее, но вместо 2ого “name”
вставьте ваше имя.
4. Есть строка: test_string = "Hello world!", необходимо
напечатать на каком месте находится буква w
кол-во букв l
Проверить начинается ли строка с подстроки: “Hello”
Проверить заканчивается ли строка подстрокой: “qwe”
"""

random_string_1 = "Hello, it's me"
print(random_string_1[0])
print(random_string_1[-1])
print(random_string_1[2])
print(random_string_1[-3])
print(len(random_string_1), end='\n\n')


random_string_2 = "I was wondering if after"
print(random_string_2[:8])
print(random_string_2[int((len(random_string_2) / 2) - 2):
                      int((len(random_string_2) / 2) + 2)])
print(random_string_2[::3])
print(random_string_2[::-1], end='\n\n')


random_string_3 = "my name is name"
rr = random_string_3.split()
rr[3] = 'Nikita'
new_string = ' '.join(rr)
print(new_string, end='\n\n')


test_string = "Hello world!"
print(test_string.find('w'))
print(test_string.count('l'))
print(test_string.startswith("Hello"))
print(test_string.endswith("qwe"))
