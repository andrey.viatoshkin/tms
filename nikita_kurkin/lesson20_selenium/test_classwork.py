from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from .locators import WikiPageLocators


class TestWikiPage:
    def test_check_search_result(self, browser):
        browser.get('https://en.wikipedia.org/wiki/Main_Page')
        # search_filed = browser.find_element(By.ID, "searchInput")

        search_filed = browser.find_element(*WikiPageLocators.SEARCH_INPUT)
        search_filed.send_keys('Python')
        search_filed.send_keys(Keys.ENTER)
        search_header = browser.find_element(By.ID, "firstHeading")
        assert search_header.text == "Python", "Header is not contain 'python'"
