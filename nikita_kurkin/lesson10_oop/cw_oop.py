"""
Создать класс Goods, в котором содержиться атрибуты: price, discount.
и метод set_discount

Унаследовать класс Food и Tools от Goods
Унаследовать класс Banana, Apple, Cherry от класса Food
Унаследовать класс Ham, Nail, Axe от класса Tools
"""


class Goods:

    def __init__(self, price, discount):
        self.price = price
        self.discount = discount

    def set_discount(self):
        return self.price * (1 - self.discount)


class Food(Goods):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Tools(Goods):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Banana(Food):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Apple(Food):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Cherry(Food):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Ham(Tools):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Nail(Tools):
    def __init__(self, price, discount):
        super().__init__(price, discount)


class Axe(Tools):
    def __init__(self, price, discount):
        super().__init__(price, discount)


"""
Создать класс Store и добавить в него словарь, где будет храниться item: price.
Реализовать в классе методы add_item, remove_item
Реализовать метод overall_price_discount который считает
полную сумму со скидкой добавленных в магазин товаров
Реализовать метод overall_price_no_discount который
считает сумму товаров без скидки. Реализовать классы
GroceryStore и HardwareStore и унаследовать эти классы от Store
Реализовать в GroceryStore проверку, что объект, который был передан в класс
пренадлежит классу Food, HardwareStore Tools
"""


class Store:
    items = {}

    def __init__(self, item, price):
        self.item = item
        self.price = price
        self.add_item()

    def add_item(self):
        Store.items.update({self.item: self.price})

    def remove_item(self):
        Store.items.pop(self.item)

    def overall_price_discount(self):
        price_dics = sum(Store.items.values()) - sum(Store.
                                                     items.values()) * 0.10
        return price_dics

    def overall_price_no_discount(self):
        price_without_dics = sum(Store.items.values())
        return price_without_dics


class GroceryStore(Store):  # food
    def __init__(self, instance, item, price):
        super().__init__(item, price)
        if isinstance(instance, Food):
            print('ok')
        else:
            raise TypeError("instance must belong to the Food class")


class HardwareStore(Store):  # Tools
    def __init__(self, instance, item, price):
        super().__init__(item, price)
        if isinstance(instance, Tools):
            print('ok')
        else:
            raise TypeError("instance must belong to the Tools class")


tomato = Food('tomato', 100)
hammer = Tools('hammer', 200)

gr_tomato = GroceryStore(tomato, 'tomato', 100)
hw_hammer = HardwareStore(hammer, 'hammer', 200)

print(issubclass(GroceryStore, Food))
print(issubclass(HardwareStore, Tools))
