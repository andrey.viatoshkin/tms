# Set legs of a right triangle.
a, b = 3, 4

# Define variables for the hypotenuse and the area of a right triangle.
c = (a ** 2 + b ** 2) ** 0.5
s = 0.5 * (a * b)

print(c, s, sep='\n')
