# This module contains the function for choosing one the programs to launch.
# It can launch either Caesar cipher encoder/decoder or bank deposit
# calculator. After 4 wrong attempts to select a program, the function
# launches one of these modules at random.
# The module allows to re-select the program to run after the program
# execution.
from random import randint
from package_1.bank_deposit import run_deposit
from package_2.Caesar_cipher import run_cipher


def select_module():
    """
    This function launches the selected program. On wrong input, the first
    3 attempts - show an error message, the 4th one - launch a random program.
    """
    attempt = 1
    while True:
        print('Select the program to launch:')
        choice = input('1 - Calculate bank deposit\n'
                       '2 - Caesar cipher encoder/decoder\n'
                       'Input (1/2): ')
        if attempt == 4 and choice not in '12':
            choice = str(randint(1, 2))
            print('4 wrong attempts. A random program is launched.')
        if choice == '1':
            run_deposit()
            break
        elif choice == '2':
            run_cipher()
            break
        else:
            print('Wrong input.')
            attempt += 1


while True:
    select_module()
    flag = False
    again = input('Do you want to run another program? (y/n): ')
    while again not in "yn":
        again = input('Enter "y" to run the program, or "n" to exit: ')
    if again == "n":
        exit()
